<section>
    <div class="lSSlideOuter ">
        <div class="lSSlideWrapper usingCss" style="transition-duration: 3000ms; transition-timing-function: ease;">
            <ul id="slidemain" class="lightSlider lsGrab lSSlide">
                <li> <img src="<?=base_url();?>images/banner-ir.jpg" width="100%"> </li>
            </ul>
            <div class="lSAction" style=""><a class="lSPrev"></a><a class="lSNext"></a></div>
        </div>
    </div>
</section>

<section class="position-welcome">
    <div class="grid-container display-welcome">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 crop-banner-left">
                <h1 class="font-white" style="margin: 0;">ยินดีต้อนรับสู่</h1>
                <h1 class="font-white"><strong>นักลงทุนสัมพันธ์</strong></h1>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 crop-banner-right">
                <p class="font-white">นักลงทุนสัมพันธ์เป็นสื่อกลางในการสื่อสารข้อมูลสู่สาธารณะ
                    เพื่อสร้างความเข้าใจที่ชัดเจนแม่นยำและเป็นประโยชน์ต่อนักลงทุนสัมพันธ์</p>
            </div>
        </div>
    </div>
</section>

<section class="text-center section-stock">
    <div class="grid-container stock-frame">
        <div class="col-lg-4 col-md-4 pad-stock frame-left-stock">
            <div style="display: flex;">
                <div class="frame-img-stock"> <img src="<?=base_url();?>images/logo.png" class="img-stock"> </div>
                <div class="frame-stock">
                    <h6 class="h6-stock">ตลาด : <span class="font-mint-green"
                            style="text-decoration: underline;">SET</span></h6>
                    <h6 class="h6-stock">กลุ่มอุตสาหกรรม : <span class="font-mint-green"
                            style="text-decoration: underline;">อสังหาริมทรัพย์และก่อสร้าง</span></h6>
                    <h6 class="h6-stock">หมวดธุรกิจ : <span class="font-mint-green"
                            style="text-decoration: underline;">พัฒนาอสังหาริมทรัพย์</span></h6>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 pad-stock frame-center-stock">
            <div class="col-lg-4 col-md-4 align-center-stock">
                <h4 class="font-mint-green" style="margin: 0;"><strong>RICHY</strong></h4>
                <h6 class="h6-center-stock"><strong>ชื่อหุ้น</strong></h6>
            </div>
            <div class="col-lg-4 col-md-4 align-center-stock">
                <h4 class="font-mint-green" style="margin: 0;"><strong>10.33</strong></h4>
                <h6 class="h6-center-stock"><strong>ผลตอบแทนสินทรัพย์ (ROA) <br>
                        (%)</strong></h6>
            </div>
            <div class="col-lg-4 col-md-4 align-center-stock">
                <h4 class="font-mint-green" style="margin: 0;"><strong>21.59</strong></h4>
                <h6 class="h6-center-stock"><strong>ผลตอบแทนต่อผู้ถือหุ้น (ROE) <br>
                        (%)</strong></h6>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 pad-stock frame-right-stock">
            <div class="align-right-stock">
                <h6 class="font-white margin-right-stock">อัตรากำไรสุทธิ <span class="right-sub-stock">16.65%</span>
                </h6>
                <h6 class="font-white margin-right-stock">กำไรสุทธิ <span class="right-sub-stock">451.02 ล้านบาท</span>
                </h6>
                <h6 class="h6-stock font-white right-sub-stock">ข้อมูลล่าสุด : 31 ธันวาคม 2561</h6>
            </div>
        </div>
    </div>
</section>


<!-- ................................................ Menu left ........................................................... -->

<div class="mbr-section article section-menu">
    <div class="container-design">
        <div class="frame-menu">
            <div id="dl-menu" class="dl-menuwrapper col-lg-8 col-md-6 col-sm-12 col-xs-12">
                <button class="dl-trigger" style="animation-name: fadeInLeft !important;">Open Menu</button>
                <p><strong>เมนูนักลงทุนสัมพันธ์</strong></p>
                <ul class="dl-menu">
                    <li class="active"><a href="<?=site_url();?>investor">หน้าหลักนักลงทุนสัมพันธ์</a></li>
                    <li><a href="<?=site_url();?>investor/#">ข้อมูลบริษัท</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/chairman">สารจากประธานกรรมการ</a></li>
                            <li><a href="<?=site_url();?>investor/profile">ประวัติความเป็นมา</a></li>
                            <li><a href="<?=site_url();?>investor/passion">วิสัยทัศน์และพันธกิจ</a></li>
                            <li><a href="<?=site_url();?>investor/board">คณะกรรมการบริษัทและผู้บริหาร</a></li>
                            <li><a href="<?=site_url();?>investor/organization">โครงสร้างองค์กร</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=site_url();?>investor/governance">การกำกับดูแลกิจการ</a></li>
                    <li><a href="<?=site_url();?>investor/#">ข้อมูลทางการเงิน</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/finance_statement">งบการเงิน</a></li>
                            <li><a href="<?=site_url();?>investor/financial_highlight">ข้อมูลสำคัญทางการเงิน</a></li>
                            <li><a href="<?=site_url();?>investor/mda">คำอธิบายและการวิเคราะห์</a></li>
                            <li><a href="<?=site_url();?>investor/annual_report">รายงานประจำปี</a></li>
                            <li><a href="<?=site_url();?>investor/finance_56">แบบ 56-1</a></li>
                            <li><a href="http://capital.sec.or.th/webapp/corp_fin2/cgi-bin/final69.php?txt_compid=1388&amp;txt_offerid=001163&amp;txt_secuid=01&amp;txt_offertypeid=01&amp;txt_language=T&amp;txt_applydate=2013-11-04&amp;start_date=2014-07-28"
                                    target="_blank">หนังสือชี้ชวน</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=site_url();?>investor/#">ข้อมูลสำหรับผู้ถือหุ้น</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/corporate">ข้อมูลทั่วไปของบริษัท</a></li>
                            <li><a href="<?=site_url();?>investor/major">โครงสร้างผู้ถือหุ้น</a></li>
                            <li><a href="<?=site_url();?>investor/general_meeting">การประชุมผู้ถือหุ้น</a></li>
                            <li><a href="<?=site_url();?>investor/dividend">นโยบายและการจ่ายเงินปันผล</a></li>
                            <li><a href="<?=site_url();?>investor/factsheet" target="_blank">สรุปข้อสนเทศ</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=site_url();?>investor/webcasts">ข้อมูลนำเสนอแบบมัลติมีเดีย</a></li>
                    <li><a href="<?=site_url();?>investor/analyst">บทวิเคราะห์</a></li>
                    <li><a href="<?=site_url();?>investor/#">ข่าวสารและกิจกรรม</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/ir_calendar">ปฏิทินกิจกรรม</a></li>
                            <li><a href="<?=site_url();?>investor/news_update">ข่าวสารล่าสุด</a></li>
                            <li><a href="<?=site_url();?>investor/news_clipping">ข่าวจากหนังสือพิมพ์</a></li>
                            <li><a href="<?=site_url();?>investor/set_announcement">ข่าวประกาศจากตลาดหลักทรัพย์</a></li>
                            <li><a href="<?=site_url();?>investor/publicize">ข่าวประชาสัมพันธ์</a></li>
                            <li><a href="<?=site_url();?>investor/press">ข่าวเผยแพร่</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=site_url();?>investor/#">ข้อมูลราคาหลักทรัพย์</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/stock_price">ราคาหลักทรัพย์ล่าสุด</a></li>
                            <li><a href="https://www.set.or.th/set/historicaltrading.do?symbol=RICHY&amp;ssoPageId=2&amp;language=th&amp;country=TH"
                                    target="_blank">ราคาหลักทรัพย์ย้อนหลัง</a></li>
                            <li><a href="<?=site_url();?>investor/calculator">เครื่องคำนวณการลงทุน</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=site_url();?>investor/ir_contact">ติดต่อนักลงทุนสัมพันธ์</a></li>
                    <li><a href="<?=site_url();?>investor/#">สอบถามข้อมูล</a>
                        <ul class="dl-submenu">
                            <li><a href="<?=site_url();?>investor/request_alerts">ระบบแจ้งเตือนทางอีเมล์</a></li>
                            <li><a href="<?=site_url();?>investor/request_inquiry">ฝากคำถาม</a></li>
                            <li><a href="<?=site_url();?>investor/faqs">คำถามที่ถูกถามบ่อย</a></li>
                            <li><a href="<?=site_url();?>investor/complaints">ช่องทางรับเรื่องร้องเรียน</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="col-lg-2 col-md-3 offset-menu-th width-short-th">
                <div style="margin: 0 auto; display: -webkit-box;">
                    <a href="<?=site_url();?>investor/complaints" style="display: flex;">
                        <img class="img-short" src="<?=base_url();?>images/complaint-short.png">
                        <p class="text-short">ช่องทางรับเรื่องร้องเรียน</p>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3 width-short-th" style="border-right: 1px solid #047c49;">
                <div style="margin: 0 auto; display: -webkit-box;">
                    <a href="<?=site_url();?>investor/ir_contact" style="display: flex;">
                        <img class="img-short" src="<?=base_url();?>images/mail-short.png">
                        <p class="text-short">ติดต่อนักลงทุนสัมพันธ์</p>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ............................................... End Menu left ........................................................ -->
