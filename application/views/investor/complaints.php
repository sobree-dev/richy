<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        table:not([class]):not([id]) td {
            padding: 4px 1px !important;
            border: 0px !important;
            background-color: #FFFFFF;
        }

        table {
            width: 90%;
            padding: 0 !important;
            margin: 0 0 0px 0 !important;
            font-size: 13px;
        }

        td {
            padding: 0px 0px !important;

        }

        #check_box td {
            background-color: #d1f5e3;

        }

        .size_button {
            font-family: Kanit, sans-serif;
            background-color: #0d9748;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        .size_button:hover {
            font-family: Kanit, sans-serif;
            background-color: #76d49e;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        table thead,
        table tbody,
        table tfoot {
            border: transparent;
        }

        #submit {
            background: #1e7154;
            color: #ffffff;
            border: 1px solid #009a49;
            font-size: 14px !important;
            font-style: normal !important;
            font-weight: 300 !important;
            padding: 10px 30px;
            cursor: pointer;
        }

        .active {
            font-size: inherit !important;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail" style="max-width: 1200px!important;">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>ช่องทางรับเรื่องร้องเรียน</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu"
                        href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span>สอบถามข้อมูล</span><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span
                    class="font-mint-green">ช่องทางรับเรื่องร้องเรียน</span></p>
            <br><br>
            <table cellSpacing="0" cellPadding="0" border="0" width="80%" align="center"
                style="max-width: 1200px!important;">
                <tr>
                    <td>
                        <!-- ++++++++++++++++++++++++  content ++++++++++++++++++++++++-->
                        <div class="webcasts">
                            <form action="<?=site_url('investor/complaints_save');?>" method='post' name="frm1"
                                enctype="multipart/form-data">
                                <table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center"
                                    class="bg_email_alert5">
                                    <tr>
                                        <td align="center">
                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left">

                                                                    เมื่อท่านไม่ได้รับความเป็นธรรมในการลงทุน
                                                                    หรือพบเห็นการกระทำผิด
                                                                    การกระทำอันไม่เป็นธรรมเกี่ยวกับบริษัท และ/หรือ
                                                                    ผู้ลงทุน โดยคณะกรรมการตรวจสอบ และ/หรือ
                                                                    คณะกรรมการบริษัท<br>
                                                                    จะดำเนินการตรวจสอบเรื่องร้องเรียนและเบาะแสที่ท่านแจ้งมา
                                                                    เพื่อให้มีความยุติธรรมแก่ผู้ลงทุนและบริษัทโดยเร็วที่สุด<br><br>

                                                                    กรุณากรอกข้อมูลให้ครบถ้วนโดยข้อมูลนี้จะถูกส่งไปยัง
                                                                    คณะกรรมการตรวจสอบ และ/หรือ คณะกรรมการบริษัท
                                                                    ที่ได้รับการแต่งตั้งจาก CEO
                                                                    ข้อมูลของท่านจะถูกปกปิดเป็นความลับสูงสุด<br><br><br>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0"
                                                                        cellpadding="10">
                                                                        <tr>
                                                                            <td width="30%" align="left" valign="top">
                                                                                <b>ประเภทการรับเรื่องร้องเรียน<font color="red"> *</font></b>
                                                                            </td>
                                                                            <td align="left" width="2%" valign="top">:
                                                                            </td>
                                                                            <td align="left" valign="top">
                                                                                <select name="catagory" id="catagory"
                                                                                    size="1" style="width: 300px;"
                                                                                    class="style_combo" required>
                                                                                    <option selected value="">กรุณาเลือกหัวข้อการร้องเรียน *</option>
                                                                                    <?php
                                                                                    $complaints_category = $this->db->get('complaints_category');
                                                                                    foreach ($complaints_category->result_array() as $value) {
                                                                                    ?>
                                                                                    <option
                                                                                        value="<?=$value['title_'.$this->lang->lang()];?>">
                                                                                        <?=$value['title_'.$this->lang->lang()];?>
                                                                                    </option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>รายละเอียด<font color="red"> *</font>
                                                                                </b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <textarea name="description"
                                                                                    id="description" type="textarea"
                                                                                    cols="35" rows="3"
                                                                                    class="style_text_area"
                                                                                    wrap="virtual" value=""
                                                                                    required></textarea>
                                                                                <br>กรุณาใส่ข้อความไม่เกิน 3000 ตัวอักษร
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>เอกสารแนบ</b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <!----------------------------------------  Upload File ---------------------------------------->
                                                                                <div
                                                                                    class="custom-control custom-checkbox my-1 mr-sm-2">
                                                                                    <label class="custom-control-label"
                                                                                        for="">Attach File 1</label>
                                                                                    <input type="file" id="file1"
                                                                                        name="file1"
                                                                                        class="form-control-file border" />
                                                                                </div>
                                                                                <div
                                                                                    class="custom-control custom-checkbox my-1 mr-sm-2">
                                                                                    <label class="custom-control-label"
                                                                                        for="">Attach File 2</label>
                                                                                    <input type="file" id="file2"
                                                                                        name="file2"
                                                                                        class="form-control-file border" />
                                                                                </div>
                                                                                <div
                                                                                    class="custom-control custom-checkbox my-1 mr-sm-2">
                                                                                    <label class="custom-control-label"
                                                                                        for="">Attach File 3</label>
                                                                                    <input type="file" id="file3"
                                                                                        name="file3"
                                                                                        class="form-control-file border" />
                                                                                </div>
                                                                                <!-- <IFRAME name="frameUpload"
                                                                                    id="frameUpload"
                                                                                    src="<?=base_url('attach_file/default.php');?>"
                                                                                    frameBorder="0" width="100%"
                                                                                    height="auto"
                                                                                    scrolling="no"></IFRAME> -->
                                                                                <!-----------------------------------------  End Upload File ---------------------------------->


                                                                                <br>รองรับไฟล์ .doc .xls .pdf .jpeg .tif (ขนาดไม่เกิน 10MB) <br> และไม่อนุญาติให้อัพโหลดไฟล์ .exe

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" colspan="3">
                                                                                <br><b>ช่องทางติดต่อกลับผู้ร้องเรียน</b><br><br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>ชื่อ<font color="red"> *</font></b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <input name="name" id="name" type="text"
                                                                                    size="35" maxlength="50"
                                                                                    class="style_textbox" value=""
                                                                                    required>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>เบอร์โทรศัพท์</b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <input name="telphone" id="telphone"
                                                                                    type="text" size="35" maxlength="50"
                                                                                    class="style_textbox" value="">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>อีเมล์<font color="red"> *</font></b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <input name="email" id="email"
                                                                                    type="email" size="35"
                                                                                    maxlength="100"
                                                                                    class="style_textbox" value=""
                                                                                    required>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <b>ช่องทางอื่นๆ</b>
                                                                            </td>
                                                                            <td align="left" valign="top">:</td>
                                                                            <td align="left" valign="top">
                                                                                <textarea name="other" id="other"
                                                                                    type="textarea" cols="35" rows="4"
                                                                                    class="style_text_area"
                                                                                    wrap="virtual" value=""></textarea>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td align="left" valign="top">
                                                                                <!--<a href="#" class="button-agm et_pb_more_button et_pb_button" onclick="javascript: return chk_complaints_info();">Submit</a>-->
                                                                                <input id="submit" type="submit"
                                                                                    name="Action" value="ส่งข้อมูล"
                                                                                    onclick="javascript: return chk_complaints_info();">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" colspan="3">
                                                                                <br><br>
                                                                                <font color="red">* หมายเหตุ กรุณากรอกข้อมูลที่จำเป็น</font><br><br>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <!-- ++++++++++++++++++++++ end content +++++++++++++++++++++++-->
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->

<script>
    jQuery("input[type=file]").change(function (event) {

        //alert(jQuery("#" + this.id).val())
        var file, extension, filetype, img, filename;
        var flagfile = true;
        var allowedExtensions = ["png", "jpg", "jpeg", "gif", "tif", "tiff", "pdf", "doc", "xls", "docx",
            "xlsx"];

        if ((file = this.files[0])) {

            filetype = file.name;
            extension = filetype.substr(filetype.lastIndexOf('.') + 1).toLowerCase();
            //alert(file.name)
            if (allowedExtensions.indexOf(extension) == -1) {
                flagfile = false;
                alert(
                    "รองรับไฟล์ .doc .xls .pdf .jpeg .tif (ขนาดไม่เกิน 10MB) และไม่อนุญาติให้อัพโหลดไฟล์ .exe");
            }

            // 2097152 Byte = 2 MB
            if (file.size > 10485760) { //ขนาดไม่เกิน 10 MB
                alert(
                    "รองรับไฟล์ .doc .xls .pdf .jpeg .tif (ขนาดไม่เกิน 10MB) และไม่อนุญาติให้อัพโหลดไฟล์ .exe");
                flagfile = false;
            }

            if (flagfile == false) {
                jQuery("input[type=file]").val("");
                return false;
            } else {

                var formData = new FormData();
                formData.append('file', jQuery('#' + this.id)[0].files[0]);
                console.log(jQuery('#' + this.id)[0].files[0]);
                console.log('fileUploader.asp?uploadpath=' + jQuery('#folderpath').val() + '&filename=' +
                    jQuery('#hid_attach_file_1').val());
                jQuery.ajax({
                    type: 'post',
                    url: 'fileUploader.asp?uploadpath=' + jQuery('#folderpath').val() + '&filename=' +
                        jQuery('#hid_attach_file_1').val(),
                    data: formData,
                    success: function (status) {

                        if (button_id == 1) {
                            jQuery('#hid_attach_file_1').val(file.name)
                        } else if (button_id == 2) {
                            jQuery('#hid_attach_file_2').val(file.name)
                        } else if (button_id == 3) {
                            jQuery('#hid_attach_file_3').val(file.name)
                        }
                    },
                    processData: false,
                    contentType: false,
                    error: function () {
                        //alert("Whoops something went wrong!");
                    }
                });

            }

        }

    });
</script>