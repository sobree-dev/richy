<!---------------------------------- Content ---------------------------------------->
<style>
td {
    padding: 20px !important;
    line-height: 20px;
}
</style>

<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('BoardofDirector')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('CorporateInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('BoardofDirector')?></span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <?php
                $board = $this->db->get('board');
                foreach ($board->result_array() as $value) {
                ?>
                <div style="width: 100%;">
                    <p class="text_09"><?=$value['title_'.$this->lang->lang()];?></p>
                    <p><?=$value['detail_'.$this->lang->lang()];?></p>
                    <div class="table-data">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <th width="9%" align="center">No.</th>
                                    <th width="49%" align="center">Name</th>
                                    <th width="42%" align="center">Position</th>
                                </tr>
                                <?php
                                $key = 1;
                                $this->db->where('board_id', $value['id']);
                                $board_list = $this->db->get('board_list');
                                foreach ($board_list->result_array() as $value) {
                                ?>
                                <tr>
                                    <td align="center"><?=$key?></td>
                                    <td align="left"><?=$value['title_'.$this->lang->lang()];?></td>
                                    <td align="left"><?=$value['position_'.$this->lang->lang()];?></td>
                                </tr>
                                <?php $key++; } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- / table-data -->
                    <br>
                    <br>
                </div>
                <?php } ?>

                <!-- / twelve columns -->
            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->