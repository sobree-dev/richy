<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Financial Highlight</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Financial Info</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Financial Highlight</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <div class="twelve columns">
                    <p>Summary of financial position and company results for the past 3 years <span
                            class="font-mint-green" style="float:right;margin-bottom:0px;"><strong>(Unit:
                                M.Baht)</strong></span></p>
                    <div class="table-data">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <th width="380" align="center">Financial Data &amp; Ratio</th>
                                    <th width="140" align="center">2016</th>
                                    <th width="140" align="center">2017</th>
                                    <th width="140" align="center">2018</th>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Assets</td>
                                    <td width="140" align="center">5,570.31</td>
                                    <td width="140" align="center">6,062.54</td>
                                    <td width="140" align="center"> 5,119.47 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Liabilities</td>
                                    <td width="140" align="center">4,049.95</td>
                                    <td width="140" align="center"> 4,195.06 </td>
                                    <td width="140" align="center"> 2,809.45  </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Equity</td>
                                    <td width="140" align="center">1,520.37</td>
                                    <td width="140" align="center"> 1,867.48 </td>
                                    <td width="140" align="center"> 2,310.02 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Paid-up Capital</td>
                                    <td width="140" align="center">785.40</td>
                                    <td width="140" align="center"> 975.20 </td>
                                    <td width="140" align="center"> 1,044.86 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Revenue</td>
                                    <td width="140" align="center">568.24</td>
                                    <td width="140" align="center"> 1,327.13 </td>
                                    <td width="140" align="center"> 2,708.65 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Net Profit</td>
                                    <td width="140" align="center">9.26</td>
                                    <td width="140" align="center"> 132.92 </td>
                                    <td width="140" align="center"> 451.02 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">EPS (Baht)</td>
                                    <td width="140" align="center">0.01</td>
                                    <td width="140" align="center"> 0.14 </td>
                                    <td width="140" align="center"> 0.43 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Return on Asset (ROA) : %</td>
                                    <td width="140" align="center">0.80</td>
                                    <td width="140" align="center"> 3.51 </td>
                                    <td width="140" align="center"> 10.33 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Return on Equity (ROE) : %</td>
                                    <td width="140" align="center">0.60</td>
                                    <td width="140" align="center"> 7.85 </td>
                                    <td width="140" align="center"> 21.59 </td>
                                </tr>
                                <tr>
                                    <td width="380" align="left">Net Profit Margin(%)</td>
                                    <td width="140" align="center">1.63</td>
                                    <td width="140" align="center"> 10.02 </td>
                                    <td width="140" align="center"> 16.65 </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->