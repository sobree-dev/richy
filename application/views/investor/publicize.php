<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('PublicRelations')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu"
                        href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span><?php echo lang('NewsActivities')?></span><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('PublicRelations')?></span></p>
        </div>
        <br>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <!------------------------------------ Content ------------------------------------------->
                <form name="frm1" method="POST" action="public.asp">

                    <?php
                    $publicize = $this->db->get('publicize');
                    foreach ($publicize->result_array() as $value) {
                    ?>
                    <div class="col-md-6" align="center" style="padding-bottom:20px;">
                        <div style="border: 1px solid rgb(234, 234, 233);padding: 5px 25px;">
                            <div>
                                <div>
                                    <a href="#">
                                        <img src="<?=base_url('uploads/publicize/'.$value['image']);?>"
                                            style="max-width:390px;height: 245px;object-fit: cover;"><br><br>
                                    </a>
                                </div>
                                <div style="text-align: left;">
                                    <div style="height: 39px;overflow: hidden;">
                                    <b style="color: rgb(13, 61, 33);font-size: 23px;"><?=$value['title_'.$this->lang->lang()];?></b>
                                    </div>
                                    <div style="height: 80px;overflow: hidden;color: #9D9D9D;">
                                        <?=$value['detail_'.$this->lang->lang()];?>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="text-align: left;">
                                    <span style="color: rgb(13, 61, 33);"><?=$value['date'];?></span>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                    <a href="<?=site_url('investor/publicize_detail/?topic=publicize&param='.$value['id']);?>" target="_blank" style="color: rgb(13, 61, 33);font-weight: 700;">
                                        เพิ่มเติม<i class="glyphicon glyphicon-menu-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php } ?>




                    <!--<div style="width:100%;" align="center">
											<table width="100%" cellpadding="0" cellspacing="0" border="0">
												<tr>
													<td align="center">
														<br><br>
														<!------------------------------------------- Page Button ----------------------------------------->
                    <!-- <div align="center">
                        <div class="paginator">
                            <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                                <input type="hidden" id="totalpage" name="totalpage" value="1">
                                <input type="hidden" id="pageno" name="pageno" value="1">
                                <input type="hidden" id="pageno_first" name="pageno_first" value="">
                                <input type="hidden" name="date" id="date" value="">
                                <input type="hidden" name="chkFrist" id="chkFrist" value="">
                                <script language="javascript">
                                    function SelectedPageChanged(page, selectedPage) {
                                        document.getElementById("pageno").value = selectedPage;
                                        document.forms["frm1"].submit();
                                    }

                                    function SelectedPageChangedMore(page, selectedPage) {
                                        document.getElementById("pageno_first").value = selectedPage;
                                        document.getElementById("pageno").value = selectedPage;
                                        document.forms["frm1"].submit();
                                    }

                                    function SelectedPageChangedNext(page, selectedPage) {
                                        document.getElementById("pageno_first").value = selectedPage;
                                        document.getElementById("pageno").value = selectedPage;
                                        document.forms["frm1"].submit();
                                    }

                                    function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                        document.getElementById("pageno_first").value = constPageFrist;
                                        document.getElementById("pageno").value = selectedPage;
                                        document.forms["frm1"].submit();
                                    }
                                </script>
                            </div>
                        </div>
                    </div> -->
                    <br>
                    <!------------------------------------- End Page Button ----------------------------------------->
                    <!--</td>
												</tr>
											</table>
										</div>-->

                </form>

                <!--------------------------------- End Content ------------------------------------------>
            </div>


        </div>
    </div>
</section>