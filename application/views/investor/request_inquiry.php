<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        table:not([class]):not([id]) td {
            padding: 4px 1px !important;
            border: 0px !important;

        }

        table {
            width: 100%;
            padding: 0 !important;
            margin: 0 0 0px 0 !important;
            font-size: 13px;


        }

        td {
            padding: 0px 0px !important;
            background-color: #FFFFFF;
        }

        #check_box td {
            background-color: #d1f5e3;

        }

        .size_button {
            font-family: Kanit, sans-serif;
            background-color: #0d9748;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        .size_button:hover {
            font-family: Kanit, sans-serif;
            background-color: #76d49e;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        table thead,
        table tbody,
        table tfoot {
            border: transparent;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Inquiry Form</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Info Request</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Inquiry Form</span></p>
        </div>
        <table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center">
            <form name="frm1" method="post" action="https://www.irplus.in.th/Listed/RICHY/request_inquiry.asp">
                <tr>
                    <td align="center" class="normal_font">

                        <table cellSpacing="0" cellPadding="0" border="0" width="98%" align="center">





                            <tr>
                                <td valign="top">
                                    <table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center">
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="<?=base_url();?>images/green_ball.png" align="absmiddle" width="12">
                                                <font class="font_inquiry4"> Questions</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><br>

                                                <table cellSpacing="0" cellPadding="0" border="0" width="100%"
                                                    align="center">
                                                    <tr height="2px">
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>

                                                    <tr>
                                                        <td align="center" colspan="4" valign="middle"><br><br><br>
                                                            <font class="no_information">
                                                                No Information Now</font>
                                                        </td>
                                                    </tr>

                                                    <br><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="55%">
                        <table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center">
                            <tr>
                                <td align="left" valign="top">
                                    <img src="<?=base_url();?>images/green_ball.png" align="absmiddle" width="12">
                                    <font class="font_inquiry4">Questions to Management</font>
                                </td>
                            </tr>
                            <tr>
                                <td><br>
                                    <table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center">
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                    align="center">
                                                    <tr>
                                                        <td width="10px">&nbsp;</td>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">

                                                                <tr class="bgcolor_InquiryTap">
                                                                    <td id="tap1" onclick="showTap(1);" width="50%">
                                                                        Register
                                                                    <td>
                                                                    <td id="tap2" onclick="showTap(2);" width="50%">
                                                                        Member
                                                                    <td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="3">
                                                                        <table width="100%" border="0" cellspacing="5"
                                                                            cellpadding="0" class="bg_sign_in1">
                                                                            <tr class="bg_sign_in2">
                                                                                <td>
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">

                                                                                        <tr>
                                                                                            <td>
                                                                                                <div id="displayTap1"
                                                                                                    style="DISPLAY: block">
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="5"
                                                                                                        id="displayTap1">
                                                                                                        <tr height="30px"
                                                                                                            valign="middle">
                                                                                                            <td
                                                                                                                width="20px">
                                                                                                            </td>
                                                                                                            <td
                                                                                                                align="left">
                                                                                                                <br><strong>
                                                                                                                    <font
                                                                                                                        class="font_normal">

                                                                                                                        If
                                                                                                                        this
                                                                                                                        is
                                                                                                                        the
                                                                                                                        first
                                                                                                                        time
                                                                                                                        you
                                                                                                                        are
                                                                                                                        using
                                                                                                                        the
                                                                                                                        e-mail
                                                                                                                        alert
                                                                                                                        services,
                                                                                                                        then
                                                                                                                        you
                                                                                                                        will
                                                                                                                        need
                                                                                                                        to
                                                                                                                        register.

                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td></td>
                                                                                                            <td><br>

                                                                                                                <table
                                                                                                                    width="100%"
                                                                                                                    border="0"
                                                                                                                    cellpadding="5"
                                                                                                                    cellspacing="0"
                                                                                                                    align="left">
                                                                                                                    <tr height="30px"
                                                                                                                        valign="middle">
                                                                                                                        <td align="left"
                                                                                                                            valign="top"
                                                                                                                            class="spaceline">
                                                                                                                            <p>
                                                                                                                                <font
                                                                                                                                    class="font_normal">

                                                                                                                                    Please
                                                                                                                                    provide
                                                                                                                                    your
                                                                                                                                    information
                                                                                                                                    to
                                                                                                                                    subscribe
                                                                                                                                    our
                                                                                                                                    E-mail
                                                                                                                                    Alert
                                                                                                                                    Services

                                                                                                                                </font>
                                                                                                                            </p>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td align="left"
                                                                                                                            valign="top">
                                                                                                                            <br>
                                                                                                                            <table
                                                                                                                                border="0"
                                                                                                                                cellspacing="0"
                                                                                                                                cellpadding="4">
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td
                                                                                                                                        width="150">
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;E-mail
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="e_mail"
                                                                                                                                            id="e_mail"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="40"
                                                                                                                                            class="style_textbox"
                                                                                                                                            value="">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;First
                                                                                                                                        Name
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="fname"
                                                                                                                                            id="fname"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="40"
                                                                                                                                            class="style_textbox"
                                                                                                                                            value="">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Last
                                                                                                                                        Name
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="lname"
                                                                                                                                            id="lname"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="40"
                                                                                                                                            class="style_textbox"
                                                                                                                                            value="">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="50px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Address
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><textarea
                                                                                                                                            name="address"
                                                                                                                                            id="address"
                                                                                                                                            type="textarea"
                                                                                                                                            cols="25"
                                                                                                                                            rows="3"
                                                                                                                                            class="style_text_area"
                                                                                                                                            WRAP="virtual"
                                                                                                                                            value=""></textarea>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Telphone
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="telphone"
                                                                                                                                            id="telphone"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="20"
                                                                                                                                            class="style_textbox"
                                                                                                                                            value="">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Company
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="company"
                                                                                                                                            id="company"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="40"
                                                                                                                                            class="style_textbox"
                                                                                                                                            value="">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="top"
                                                                                                                                    height="28px"
                                                                                                                                    valign="middle">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Profile
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <select
                                                                                                                                            name="profile"
                                                                                                                                            id="profile"
                                                                                                                                            size="1"
                                                                                                                                            style="cursor:hand;"
                                                                                                                                            class="style_combo">
                                                                                                                                            <option
                                                                                                                                                selected
                                                                                                                                                value="0">
                                                                                                                                                Select
                                                                                                                                                Profile
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="2">
                                                                                                                                                Analyst
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="3">
                                                                                                                                                Employee
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="8">
                                                                                                                                                government
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="10">
                                                                                                                                                Government
                                                                                                                                                enterprise
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="9">
                                                                                                                                                Individual
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="11">
                                                                                                                                                Investor
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="5">
                                                                                                                                                Media
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="6">
                                                                                                                                                Other
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="12">
                                                                                                                                                Own
                                                                                                                                                Business
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="1">
                                                                                                                                                Shareholder
                                                                                                                                            </option>

                                                                                                                                            <option
                                                                                                                                                value="7">
                                                                                                                                                Student
                                                                                                                                            </option>

                                                                                                                                        </select>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr valign="middle"
                                                                                                                                    height="45px">
                                                                                                                                    <td>
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;Captcha
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td
                                                                                                                                        align="left">
                                                                                                                                        <iframe
                                                                                                                                            id="iframe_content_captcha"
                                                                                                                                            name="iframe_content_captcha"
                                                                                                                                            src="i_content_captcha.html"
                                                                                                                                            width="270px"
                                                                                                                                            height="30px"
                                                                                                                                            frameborder="0"
                                                                                                                                            framespacing="0"
                                                                                                                                            framepadding="0"
                                                                                                                                            scrolling="no"></iframe>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>

                                                                                                <div id="displayTap2"
                                                                                                    style="DISPLAY: none">
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellpadding="5"
                                                                                                        cellspacing="0"
                                                                                                        align="left">
                                                                                                        <tr height="50px"
                                                                                                            valign="middle">
                                                                                                            <td
                                                                                                                width="20px">
                                                                                                            </td>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <b><br>
                                                                                                                    <p>
                                                                                                                        <font
                                                                                                                            class="font_normal">

                                                                                                                            Enter
                                                                                                                            your
                                                                                                                            E-Mail
                                                                                                                            Address
                                                                                                                            in
                                                                                                                            the
                                                                                                                            space
                                                                                                                            provided,
                                                                                                                            Log
                                                                                                                            in
                                                                                                                            IR
                                                                                                                            Services

                                                                                                                        </font>
                                                                                                                    </p>
                                                                                                                </b>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr height="30px"
                                                                                                            valign="middle">
                                                                                                            <td></td>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <table
                                                                                                                    border="0"
                                                                                                                    cellspacing="0"
                                                                                                                    cellpadding="3">
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td
                                                                                                                            valign="top">
                                                                                                                            <table
                                                                                                                                border="0"
                                                                                                                                cellspacing="0"
                                                                                                                                cellpadding="3">
                                                                                                                                <tr
                                                                                                                                    valign="top">
                                                                                                                                    <td
                                                                                                                                        align="left">
                                                                                                                                        <font
                                                                                                                                            color="red">
                                                                                                                                            <b>*</b>
                                                                                                                                        </font>
                                                                                                                                        &nbsp;E-mail
                                                                                                                                        :
                                                                                                                                    </td>
                                                                                                                                    <td><input
                                                                                                                                            name="e_mail_member"
                                                                                                                                            id="e_mail_member"
                                                                                                                                            type="text"
                                                                                                                                            size="25"
                                                                                                                                            maxlength="40"
                                                                                                                                            class="style_textbox">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <div
                                                                                                    id="displayQuestion">
                                                                                                    <table border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="100%">
                                                                                                        <tr valign="top"
                                                                                                            align="left">
                                                                                                            <td
                                                                                                                width="20px">
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <font
                                                                                                                    color="red">
                                                                                                                    <b>*</b>
                                                                                                                </font>
                                                                                                                Questions
                                                                                                                :
                                                                                                            </td>
                                                                                                            <td><textarea
                                                                                                                    name="questions"
                                                                                                                    id="questions"
                                                                                                                    type="textarea"
                                                                                                                    cols="30"
                                                                                                                    rows="3"
                                                                                                                    class="style_text_area"
                                                                                                                    WRAP="virtual"></textarea>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left">&nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    <td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td align="left" valign="top"><br>
                                                            <input type="hidden" name="flag" id="flag" value="">
                                                            <input type="button" name="ok" value="Submit"
                                                                class="style_button"
                                                                onclick="javascript: return chk_Signin_InquiryForm();">
                                                            <input type="button" name="reset" value="Reset"
                                                                class="style_button"
                                                                onclick="javascript:document.frm1.submit();">

                                                            <input type="hidden" name="action" id="action">
                                                            <input type="hidden" name="resultCaptcha "
                                                                id="resultCaptcha" value="">
                                                            <input type="hidden" name="listed_com_id "
                                                                id="listed_com_id" value="1187">
                                                            <input type="hidden" name="language" id="language"
                                                                value="E">
                                                            <input type="hidden" name="hid_email_member "
                                                                id="hid_email_member" value="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td align="left" valign="top"><br>

                                                            <strong class="font_normal">Remark !</strong> If you have
                                                            any inquiry,please provide your information and fill out
                                                            your questions
                                                            on the following items. We will respond you shortly.
                                                            <br>
                                                        </td>
                                            </td>
                                        </tr>
                                    </table>

                                    <script language="javascript">
                                        if (document.getElementById("hid_email_member").value == '') {
                                            if ('' == '2') {
                                                showTap(2);
                                            } else {
                                                showTap(1);
                                            }
                                        }

                                        function showTap(id) {
                                            var maxcount = 2;
                                            for (var i = 1; i <= maxcount; i++) {
                                                if (i == id) {
                                                    document.getElementById("tap" + i).className =
                                                    'bgcolor_InquiryTap2';
                                                    document.getElementById("displayTap" + i).style.display = 'block';
                                                } else {
                                                    document.getElementById("tap" + i).className =
                                                    'bgcolor_InquiryTap3';
                                                    document.getElementById("displayTap" + i).style.display = 'none';
                                                    document.getElementById("tap" + i).style.cursor = 'hand';
                                                }
                                            }
                                            // id = 1 คือ ลงทะเบียน , id = 2 คือ สมาชิก 
                                            document.getElementById("action").value = id;

                                        }
                                    </script>





                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        </td>
        </tr>
        </table>

        </td>
        </tr>
        </form>
        </table>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->