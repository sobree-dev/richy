<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('History')?></strong>
            </h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('CorporateInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('History')?></span></p>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <?php
                $profile_info = $this->db->get('profile_info');
                foreach ($profile_info->result_array() as $value) {
                ?>
                <div style="width: 100%;">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align:center;"><img
                            src="<?=base_url('uploads/profile_info/'.$value['image']);?>" style="margin: 30px 0;">
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <?=$value['detail_'.$this->lang->lang()];?>
                    </div>
                </div>
                <?php } ?>

                <br><br>

                <div style="width: 100%; display: grid;" class="padtop50">
                    <p class="text-profile-sub">Key milestones and achievements of the Company are listed below.</p>

                    <div class="container_demo">
                        <!-- Accordion begin -->
                        <div class="accordion_example1 smk_accordion acc_with_icon">

                            <?php
                            $profile = $this->db->get('profile');
                            foreach ($profile->result_array() as $key => $value) {
                            ?>
                            <!-- Section 1 -->
                            <div class="accordion_in <?php if ($key==0) {echo 'acc_active';}?>">
                                <div class="acc_head">
                                    <div class="acc_icon_expand"></div><?=$value['title_'.$this->lang->lang()];?>
                                </div>
                                <div class="acc_content">
                                    <!-- <p class="text_10">November</p> -->
                                    <?=$value['detail_'.$this->lang->lang()];?>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                        <!-- Accordion end -->
                    </div><!-- / container_demo -->
                </div><!-- / row -->

            </div>
        </div>
    </div>


</section>
<!---------------------------------- Content ---------------------------------------->