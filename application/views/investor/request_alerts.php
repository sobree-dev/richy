<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        table:not([class]):not([id]) td {
            padding: 4px 1px !important;
            border: 0px !important;
            background-color: #FFFFFF;
        }

        table {
            width: 100%;
            padding: 0 !important;
            margin: 0 0 0px 0 !important;
            font-size: 13px;


        }

        td {
            padding: 0px 0px !important;

        }

        #check_box td {
            background-color: #d1f5e3;

        }

        .size_button {
            font-family: Kanit, sans-serif;
            background-color: #0d9748;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        .size_button:hover {
            font-family: Kanit, sans-serif;
            background-color: #76d49e;
            height: 45px;
            border-top: 2px solid #EFEFEF;
            border-bottom: 2px solid #EFEFEF;
            border-left: 2px solid #EFEFEF;
            border-right: 2px solid #EFEFEF;
            padding: 5px;
            width: 240px;
            text-align: center !important;
            color: #FFFFFF;
        }

        table thead,
        table tbody,
        table tfoot {
            border: transparent;
        }

        #imgCaptcha {
            padding-top: 3px !important;
        }

        #resultCaptcha {
            padding-top: 8px !important;
        }

        input[type='text'] {
            background-color: #FFFFFF;
            border-top: 1px solid #BEBEBE;
            border-bottom: 1px solid #BEBEBE;
            border-left: 1px solid #BEBEBE;
            border-right: 1px solid #BEBEBE;
        }

        .style_button {
            background: #1e7154;
            color: #ffffff;
            border: 1px solid #009a49;
            font-size: 14px !important;
            font-style: normal !important;
            font-weight: 300 !important;
            padding: 10px 30px;
            cursor: pointer;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>E-Mail Alert</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Info Request</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">E-Mail Alert</span></p>
        </div>
        <div class="webcast">
            <form action="https://www.irplus.in.th/Listed/RICHY/request_alerts.asp" method='post' name="frm1">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            <table cellSpacing=0 cellPadding=0 width="95%" border=0 align="center">

                                <tr>
                                    <td align="left" valign="top">
                                        <table cellSpacing=0 cellPadding=3 width="100%" border=0 align="center">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <p class="spaceline">

                                                        Email alerts are messages that are conveniently delivered to
                                                        your email
                                                        box whenever certain new company information is posted to this
                                                        site.

                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="spaceline"><br>

                                                    To automatically receive RICHY e-mail Alerts…

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="spaceline">

                                                    1. Enter your information in the space provided.

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="spaceline">

                                                    2. Click on the "<strong>
                                                        <font color="#2B8CDD" class="font_normal">OK</font>
                                                    </strong>" button.

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="15" cellspacing="5"
                                            class="bg_email_alert4">
                                            <tr>
                                                <td align="center" valign="top" class="bg_email_alert5">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="1">

                                                        <tr align="left" valign="top" class="border-line">
                                                            <td>
                                                                <table width="100%" border="0" cellspacing="0"
                                                                    cellpadding="5">
                                                                    <tr>
                                                                        <td width="10px"></td>
                                                                        <td class="spaceline"><br>

                                                                            <strong>
                                                                                <font class="font_normal">Log in IR
                                                                                    Member Service.</font>
                                                                            </strong>

                                                                        </td>
                                                                    </tr>
                                                                    <tr align="left" valign="top">
                                                                        <td></td>
                                                                        <td>
                                                                            <table width="100%" border="0"
                                                                                cellpadding="5" cellspacing="0"
                                                                                align="left">
                                                                                <tr height="25px" valign="middle">
                                                                                    <td align="left" valign="top"
                                                                                        class="spaceline">
                                                                                        <p>
                                                                                            <font class="font_normal">

                                                                                                Enter your E-Mail
                                                                                                Address in the space
                                                                                                provided, <br>Log in IR
                                                                                                Services

                                                                                            </font>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top"
                                                                                        class="spaceline">
                                                                                        <table border="0"
                                                                                            cellspacing="0"
                                                                                            cellpadding="3">
                                                                                            <tr valign="top"
                                                                                                height="25px"
                                                                                                valign="middle">
                                                                                                <td><br>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;E-mail :
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="25px"
                                                                                                valign="middle">
                                                                                                <td><input
                                                                                                        name="e_mail_member"
                                                                                                        id="e_mail_member"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="40"
                                                                                                        class="style_textbox"
                                                                                                        value=""></td>
                                                                                            </tr>

                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="bg_email_alert3"></td>
                                                            <td width="55%" align="center">
                                                                <table width="98%" border="0" cellspacing="0"
                                                                    cellpadding="5">
                                                                    <tr>
                                                                        <td align="left" valign="top" width="10"></td>
                                                                        <td class="spaceline"><br>
                                                                            <strong>
                                                                                <font class="font_normal">

                                                                                    If this is the first time you are
                                                                                    using the e-mail alert services,
                                                                                    then you will need to register.

                                                                                </font>
                                                                            </strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td>

                                                                            <table width="100%" border="0"
                                                                                cellpadding="5" cellspacing="0"
                                                                                align="left">
                                                                                <tr height="30px" valign="middle">
                                                                                    <td align="left" valign="top"
                                                                                        class="spaceline">
                                                                                        <p>
                                                                                            <font class="font_normal">

                                                                                                Please provide your
                                                                                                information to subscribe
                                                                                                our E-mail Alert
                                                                                                Services

                                                                                            </font>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top"><br>
                                                                                        <table border="0"
                                                                                            cellspacing="0"
                                                                                            cellpadding="4">
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td width="150">
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;E-mail :
                                                                                                </td>
                                                                                                <td><input name="e_mail"
                                                                                                        id="e_mail"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="40"
                                                                                                        class="style_textbox"
                                                                                                        value="">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;First Name :
                                                                                                </td>
                                                                                                <td><input name="fname"
                                                                                                        id="fname"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="40"
                                                                                                        class="style_textbox"
                                                                                                        value="">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Last Name :
                                                                                                </td>
                                                                                                <td><input name="lname"
                                                                                                        id="lname"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="40"
                                                                                                        class="style_textbox"
                                                                                                        value="">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="50px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Address :
                                                                                                </td>
                                                                                                <td><textarea
                                                                                                        name="address"
                                                                                                        id="address"
                                                                                                        type="textarea"
                                                                                                        cols="25"
                                                                                                        rows="3"
                                                                                                        class="style_text_area"
                                                                                                        WRAP="virtual"
                                                                                                        value=""></textarea>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Telphone :
                                                                                                </td>
                                                                                                <td><input
                                                                                                        name="telphone"
                                                                                                        id="telphone"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="20"
                                                                                                        class="style_textbox"
                                                                                                        value="">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Company :
                                                                                                </td>
                                                                                                <td><input
                                                                                                        name="company"
                                                                                                        id="company"
                                                                                                        type="text"
                                                                                                        size="25"
                                                                                                        maxlength="40"
                                                                                                        class="style_textbox"
                                                                                                        value="">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top"
                                                                                                height="28px"
                                                                                                valign="middle">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Profile :
                                                                                                </td>
                                                                                                <td>
                                                                                                    <select
                                                                                                        name="profile"
                                                                                                        id="profile"
                                                                                                        size="1"
                                                                                                        style="cursor:hand;"
                                                                                                        class="style_combo">
                                                                                                        <option selected
                                                                                                            value="0">
                                                                                                            Select
                                                                                                            Profile
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="2">
                                                                                                            Analyst
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="3">
                                                                                                            Employee
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="8">
                                                                                                            government
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="10">
                                                                                                            Government
                                                                                                            enterprise
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="9">
                                                                                                            Individual
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="11">
                                                                                                            Investor
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="5">
                                                                                                            Media
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="6">
                                                                                                            Other
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="12">
                                                                                                            Own Business
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="1">
                                                                                                            Shareholder
                                                                                                        </option>

                                                                                                        <option
                                                                                                            value="7">
                                                                                                            Student
                                                                                                        </option>

                                                                                                    </select>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="middle"
                                                                                                height="45px">
                                                                                                <td>
                                                                                                    <font color="red">
                                                                                                        <b>*</b></font>
                                                                                                    &nbsp;Captcha :
                                                                                                </td>
                                                                                                <td align="left"><iframe
                                                                                                        id="iframe_content_captcha"
                                                                                                        name="iframe_content_captcha"
                                                                                                        src="i_content_captcha.html"
                                                                                                        width="270px"
                                                                                                        height="30px"
                                                                                                        frameborder="0"
                                                                                                        framespacing="0"
                                                                                                        framepadding="0"
                                                                                                        scrolling="no"></iframe>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="3" align="center">

                                                                <table width="98%" border="0" cellspacing="0"
                                                                    cellpadding="0" align="center">

                                                                    <tr>
                                                                        <td align="center" valign="top"><br>
                                                                            <table width="100%" border="0"
                                                                                cellpadding="15" cellspacing="5"
                                                                                class="bg_email_alert">
                                                                                <tr>
                                                                                    <td align="center" valign="top">

                                                                                        <table width="100%" border="0"
                                                                                            cellspacing="0"
                                                                                            cellpadding="0">
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellspacing="10"
                                                                                                        cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <input
                                                                                                                    type="checkbox"
                                                                                                                    name="newsroom"
                                                                                                                    id="newsroom"
                                                                                                                    onclick="javascript:check_alert_news_room('frm1');">
                                                                                                                <strong>
                                                                                                                    <font
                                                                                                                        class="font_normal">
                                                                                                                        News
                                                                                                                        Room
                                                                                                                    </font>
                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <table
                                                                                                                    width="100%"
                                                                                                                    border="0"
                                                                                                                    cellspacing="3"
                                                                                                                    cellpadding="0">
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td width="60"
                                                                                                                            align="right">
                                                                                                                            <input
                                                                                                                                type="checkbox"
                                                                                                                                name="announce"
                                                                                                                                id="announce"
                                                                                                                                onclick="chk_newsroom('frm1');">&nbsp;
                                                                                                                        </td>
                                                                                                                        <td
                                                                                                                            valign="middle">
                                                                                                                            SET
                                                                                                                            Announcement
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td
                                                                                                                            align="right">
                                                                                                                            <input
                                                                                                                                type="checkbox"
                                                                                                                                name="calendar"
                                                                                                                                id="calendar"
                                                                                                                                onclick="chk_newsroom('frm1');">&nbsp;
                                                                                                                        </td>
                                                                                                                        <td
                                                                                                                            valign="middle">
                                                                                                                            IR
                                                                                                                            Calendar
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td
                                                                                                                            align="right">
                                                                                                                            <input
                                                                                                                                type="checkbox"
                                                                                                                                name="public_relation"
                                                                                                                                id="public_relation"
                                                                                                                                onclick="chk_newsroom('frm1');">&nbsp;
                                                                                                                        </td>
                                                                                                                        <td
                                                                                                                            valign="middle">
                                                                                                                            Public
                                                                                                                            Relations
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellspacing="10"
                                                                                                        cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <input
                                                                                                                    type="checkbox"
                                                                                                                    name="financial_info"
                                                                                                                    id="financial_info"
                                                                                                                    value="checkbox"
                                                                                                                    onclick="javascript:check_alert_financial_info('frm1');">
                                                                                                                <strong>
                                                                                                                    <font
                                                                                                                        class="font_normal">
                                                                                                                        Financial
                                                                                                                        Information
                                                                                                                    </font>
                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <table
                                                                                                                    width="100%"
                                                                                                                    border="0"
                                                                                                                    cellspacing="3"
                                                                                                                    cellpadding="0">
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td width="60"
                                                                                                                            align="right">
                                                                                                                            <input
                                                                                                                                type="checkbox"
                                                                                                                                name="financial"
                                                                                                                                id="financial"
                                                                                                                                onclick="chk_financail('frm1');">&nbsp;
                                                                                                                        </td>
                                                                                                                        <td
                                                                                                                            valign="middle">
                                                                                                                            Financial
                                                                                                                            Statement
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr align="left"
                                                                                                                        valign="top">
                                                                                                                        <td
                                                                                                                            align="right">
                                                                                                                            <input
                                                                                                                                type="checkbox"
                                                                                                                                name="annual"
                                                                                                                                id="annual"
                                                                                                                                onclick="chk_financail('frm1');">&nbsp;
                                                                                                                        </td>
                                                                                                                        <td
                                                                                                                            valign="middle">
                                                                                                                            Annual
                                                                                                                            Reports
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellspacing="10"
                                                                                                        cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <p>
                                                                                                                    <input
                                                                                                                        type="checkbox"
                                                                                                                        name="analyst"
                                                                                                                        id="analyst">
                                                                                                                    <strong>
                                                                                                                        <font
                                                                                                                            class="font_normal">
                                                                                                                            Analyst
                                                                                                                            Reports
                                                                                                                        </font>
                                                                                                                    </strong>
                                                                                                                </p>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <table width="100%"
                                                                                                        border="0"
                                                                                                        cellspacing="10"
                                                                                                        cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="top">
                                                                                                                <input
                                                                                                                    type="checkbox"
                                                                                                                    name="webcast"
                                                                                                                    id="webcast">
                                                                                                                <strong>
                                                                                                                    <font
                                                                                                                        class="font_normal">
                                                                                                                        Webcast
                                                                                                                        &amp;
                                                                                                                        Presentation
                                                                                                                    </font>
                                                                                                                </strong>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr height="5px">
                                                                        <td></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><br>
                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td width="20px"></td>
                                    <td>
                                        <input type="hidden" name="flag" value="N">
                                        <!--input type="Submit" name="ok" value="Submit" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;" onclick="javascript:return chksubmit();"-->
                                        <!--input type="Submit" name="ok" value="Reset" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;" onclick="javascript:return chkreset('');"-->
                                        <input type="button" name="ok" value="Submit" class="style_button"
                                            onclick="javascript: return chk_Signin_emailalert();">
                                        <input type="button" name="reset" value="Reset" class="style_button"
                                            onclick="javascript:document.frm1.submit();">
                                        <input type="button" name="h_alert" value="View all your e-mail alerts."
                                            class="style_button"
                                            onclick="javascript:window.open('https://www.irplus.in.th/IRPlus/EmailAlert.asp');">

                                        <input type="hidden" name="listed_com_id " id="listed_com_id" value="1187">
                                        <input type="hidden" name="language" id="language" value="E">
                                        <input type="hidden" name="hid_email_member " id="hid_email_member" value="">
                                        <input type="hidden" name="action" id="action">
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
            </form>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->