<!---------------------------------- Content ---------------------------------------->
<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('MeetingofShareholder')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('ShareholderInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('MeetingofShareholder')?></span></p>
        </div>

        <ul id='nav-sub-meeting'>
            <table width='100%' border='0' cellspacing='0' cellpadding='0' class='color_gm_head'>
                <?php
                $meeting_category = $this->db->get('meeting_category');
                foreach ($meeting_category->result_array() as $key => $value) {
                ?>
                <tr valign='top'>
                    <td>
                        <li class='sub-level-meeting'>
                            <div class='<?php if ($key==0) {echo "Hignlight";} ?>'>
                                <table border='0' width='100%' cellspacing='0' cellpadding='0'>
                                    <tr>
                                        <td class='color_gm_head_1'>
                                            <font class='fonttreeview'>&nbsp;&nbsp;<?=$value['title_'.$this->lang->lang()];?></font>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <ul id='nav-sub-meeting'>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='color_gm_head'>
                                    <?php
                                    $this->db->where('cat_id', $value['id']);
                                    $meeting_subcategory = $this->db->get('meeting_subcategory');
                                    foreach ($meeting_subcategory->result_array() as $value1) {
                                    ?>
                                    <tr valign='top'>
                                        <td>
                                            <li class='sub-level-meeting'>
                                                <div>
                                                    <table border='0' width='100%' cellspacing='1' cellpadding='5'>
                                                        <tr class='color_gm'>
                                                            <td>
                                                                <font class='fontsubhead_meeting'><?=$value1['title_'.$this->lang->lang()];?></font>
                                                            </td>
                                                            <td width='50px' align='center'></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <ul id='nav-sub-meeting' groupname='childmenu'>
                                                    <table width='100%' border='0' cellspacing='0' cellpadding='0'
                                                        class='color_gm_head'>
                                                        <?php
                                                        $this->db->where('subcat_id', $value1['id']);
                                                        $meeting = $this->db->get('meeting');
                                                        foreach ($meeting->result_array() as $value2) {
                                                            if ($value2['file']) {
                                                                $file = base_url('uploads/meeting/'.$value2['file']);
                                                                $target = '_blank';
                                                            } else {
                                                                $file = '#';
                                                                $target = '';
                                                            }
                                                        ?>
                                                        <tr valign='top'>
                                                            <td>
                                                                <li class='sub-level-meeting'>
                                                                    <div>
                                                                        <table border='0' width='100%' cellspacing='1'
                                                                            cellpadding='5'>
                                                                            <tr class='color_gm'>
                                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                                                        class='treeview'
                                                                                        href="<?=$file;?>"
                                                                                        target="<?=$target;?>" title=""><?=$value2['title_'.$this->lang->lang()];?></a></td>
                                                                                <td width='50px' align='center'><a
                                                                                        class='treeview'
                                                                                        href="<?=$file?>"
                                                                                        target="<?=$target;?>" title=""><img
                                                                                            src='<?=base_url();?>images/pdf.png'
                                                                                            width='26px'
                                                                                            align='absmiddle'
                                                                                            border='0'></a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </li>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </table>
                                                </ul>
                                            </li>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </ul>
                        </li>
                    </td>
                </tr>
                <?php } ?>

                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ul>

    </div>
</section>
<!---------------------------------- Content ---------------------------------------->