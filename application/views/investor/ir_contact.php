<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('IRContact')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('IRContact')?></span></p>
        </div>

        <?php
        $ir_contact = $this->db->get('ir_contact');
        foreach ($ir_contact->result_array() as $value) {
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padtop30 padbot50">
            <div class="row">
                <div>
                    <div class="row">
                        <div>

                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <br>
                                <img src="<?=base_url('uploads/ir_contact/'.$value['image']);?>" width="198" height="105" style="margin-bottom: 25px;">
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                <?=$value['detail_'.$this->lang->lang()];?>
                                <br>
                            </div>

                        </div>
                    </div><!-- / row -->
                </div><!-- / twelve columns -->
            </div>
        </div>
        <?php } ?>


    </div>

</section>

<style>
iframe {
    width: 100% !important;
    height: 500px !important;
}
</style>
<?php
$ir_contact = $this->db->get('ir_contact');
foreach ($ir_contact->result_array() as $value) {
?>
<div style="margin-bottom: -10px;">
    <?=$value['map'];?>
</div>
<?php } ?>

<!---------------------------------- Content ---------------------------------------->