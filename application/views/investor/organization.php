<div class="grid-container display-main">
    <?php
    $organization = $this->db->get('organization');
    foreach ($organization->result_array() as $value) {
    ?>
    <div class="pad-sub-detail">
        <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('OrganizationChart')?></strong></h1>
        <hr>
        <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                style="padding: 0 1%;">/</span><span><?php echo lang('CorporateInfo')?></strong><span class="font-gray-smoke"
                style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('OrganizationChart')?></span></p>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
        <div class="row">
            <img src="<?=base_url('uploads/organization/'.$value['image']);?>">
        </div>
    </div>
    <?php } ?>

</div>

</section>
<!---------------------------------- Content ---------------------------------------->