<!---------------------------------- Content ---------------------------------------->
<style>
    .table-data table tr td {
        padding: 20px !important;
    }
</style>

<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('Profile')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('ShareholderInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('Profile')?></span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <?php
                $corporate = $this->db->get('corporate');
                foreach ($corporate->result_array() as $value) {
                ?>
                <div>
                    <p class="text_09"><?=$value['title_'.$this->lang->lang()];?></p>
                    <div class="table-data">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <th width="277" align="center">ข้อมูล</th>
                                    <th width="51" align="center">&nbsp;</th>
                                    <th width="612" align="left">รายละเอียด</th>
                                </tr>
                                <?php
                                $this->db->where('corporate_id', $value['id']);
                                $corporate_list = $this->db->get('corporate_list');
                                foreach ($corporate_list->result_array() as $value1) {
                                ?>
                                <tr>
                                    <td width="277" align="left" valign="top"><?=$value1['title_'.$this->lang->lang()];?></td>
                                    <td width="51" align="right" valign="top">:</td>
                                    <td width="612" align="left" valign="top"><?=$value1['detail_'.$this->lang->lang()];?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->