<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        table tr td {
            padding: 20px !important;
        }
        table th {
            background-color: #1d9f68;
            font-size: 15.5px;
            color: #fff;
            border-bottom: 0px solid;
        }

        table td {
            white-space: normal !important;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('Newsclipping')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('NewsActivities')?></a></span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('Newsclipping')?></span></p>
        </div>


        <div class="webcasts">
            <div class="one columns"></div>
            <div class="ten columns">
                <form name="frm1" method="post" action="https://www.irplus.in.th/Listed/RICHY/news_clipping.asp">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="border:none !important;">
                                <!--- ........................................................... Search ........................................................... -->
                                <script type="text/javascript" src="<?=base_url();?>scripts/calendar_scripts/js/jquery-1.3.1.min.js">
                                </script>
                                <script type="text/javascript"
                                    src="<?=base_url();?>scripts/calendar_scripts/js/jquery-ui-1.7.1.custom.min.js"></script>
                                <script type="text/javascript"
                                    src="<?=base_url();?>scripts/calendar_scripts/js/daterangepicker.jQuery.js"></script>

                                <style>
                                    table,
                                    tr,
                                    th,
                                    td {
                                        border: none !important;
                                    }

                                    ::-webkit-inner-spin-button {
                                        -webkit-appearance: none;
                                    }

                                    div.col-lg-4 {
                                        position: Inherit !important;
                                    }
                                </style>
                                <link rel="stylesheet" type="text/css"
                                    href="<?=base_url();?>scripts/calendar_scripts/css/ui.daterangepicker.css">
                                <link title="ui-theme" rel="stylesheet" type="text/css"
                                    href="<?=base_url();?>scripts/calendar_scripts/css/jquery-ui-1.7.1.custom.css">
                                <link rel="Stylesheet" type="text/css" href="<?=base_url();?>scripts/calendar_scripts/css/demoPages.css"
                                    media="screen">



                                <script language="javascript">
                                    var dateStartDefault = "-1y";
                                </script>


                                <script language="javascript">
                                    jQuery.noConflict();
                                </script>

                                <br><br>
                                <div class="col-lg12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div style="padding-bottom:10px;"><span class="nomal_clipping">Start
                                                    Date&nbsp;&nbsp;
                                                    <input id="StartDate" type="date" value="2018-10-03"
                                                        required="required"></span>
                                            </div>

                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-4">
                                            <div style="padding-bottom:10px;"><span class="nomal_clipping">End
                                                    Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input id="EndDate" type="date" value="2019-10-03"
                                                        required="required"><span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-2">
                                            <a href="javascript:void(0);" class="button_search nomal_clipping"
                                                onclick="javascript: return calendar_search('frm1','E');">Search</a>
                                        </div>
                                    </div>
                                </div>


                                <input type="hidden" name="hidDateRange" id="hidDateRange" value="">
                                <input type="hidden" name="hidStartDate" value="2018-10-03">
                                <input type="hidden" name="hidEndDate" value="2019-10-03">
                                <input type="hidden" name="hidDateRangeValue" id="hidDateRangeValue" value="">


                                <!--- ....................................................... End Search ........................................................ -->



                                <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                    class="bgcolor_clipping1">
                                    <tr class="bgcolor_clipping2">
                                        <th align="center" valign="top" width="15%">
                                            <font class="clipping_font1">Date</font>
                                        </th>
                                        <th align="center" valign="top" width="50%">
                                            <font class="clipping_font1">Title</font>
                                        </th>
                                        <th align="center" valign="top" width="30%">
                                            <font class="clipping_font1">Source</font>
                                        </th>
                                    </tr>
                                    <?php
                                    $news_clipping = $this->db->get('news_clipping');
                                    foreach ($news_clipping->result_array() as $value) {
                                    ?>
                                    <tr align="left" valign="top">

                                        <td valign="top" width="20%" align="center">
                                            <font class="font_datetime2">
                                                <?=$value['date'];?>
                                            </font>
                                        </td>
                                        <td align="left" valign="top" width="50%">
                                                <a class="web"
                                                    href="<?=site_url('investor/read_detail/?topic=news_clipping&param='.$value['id']);?>"
                                                    target="_blank"><?=$value['title_'.$this->lang->lang()];?></a>
                                            <!-- &nbsp;<IMG src="<?=base_url();?>images/update.gif" border=0 align='absmiddle'> -->
                                        </td>

                                        <td align="center" valign="middle" width="30%">
                                            <img src="<?=base_url('uploads/news_clipping/'.$value['image']);?>" border="0" align='absmiddle'>
                                        </td>
                                    </tr>
                                    <?php } ?>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border:none !important;">
                                <!------------------------------------------- Page Button ----------------------------------------->
                                <!-- <div align="center">
                                    <div class="paginator">
                                        <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">

                                            <strong>Page: </strong>1 of 9 page(s) &nbsp;&nbsp;&nbsp;
                                            <span class="activepage">1</span>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','2');">
                                                <font class="font_page">2</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','3');">
                                                <font class="font_page">3</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','4');">
                                                <font class="font_page">4</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','5');">
                                                <font class="font_page">5</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','6');">
                                                <font class="font_page">6</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','7');">
                                                <font class="font_page">7</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','8');">
                                                <font class="font_page">8</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','9');">
                                                <font class="font_page">9</font>
                                            </a>

                                            <a class="page"
                                                href="javascript:SelectedPageChanged('news_clipping.html','2');">&raquo;</a>

                                            <input type="hidden" id="totalpage" name="totalpage" value="9">
                                            <input type="hidden" id="pageno" name="pageno" value="1">
                                            <input type="hidden" id="pageno_first" name="pageno_first" value="1">
                                            <input type="hidden" name="date" id="date" value="03/10/2018">
                                            <input type="hidden" name="chkFrist" id="chkFrist" value="">
                                            <script language="javascript">
                                                function SelectedPageChanged(page, selectedPage) {
                                                    document.getElementById("pageno").value = selectedPage;
                                                    document.forms["frm1"].submit();
                                                }

                                                function SelectedPageChangedMore(page, selectedPage) {
                                                    document.getElementById("pageno_first").value = selectedPage;
                                                    document.getElementById("pageno").value = selectedPage;
                                                    document.forms["frm1"].submit();
                                                }

                                                function SelectedPageChangedNext(page, selectedPage) {
                                                    document.getElementById("pageno_first").value = selectedPage;
                                                    document.getElementById("pageno").value = selectedPage;
                                                    document.forms["frm1"].submit();
                                                }

                                                function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                                    document.getElementById("pageno_first").value = constPageFrist;
                                                    document.getElementById("pageno").value = selectedPage;
                                                    document.forms["frm1"].submit();
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div> -->
                                <br>
                                <!------------------------------------- End Page Button ----------------------------------------->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!------------------------------------------- Disclaimer ----------------------------------------->
                                <!-- <font style="font-weight:normal; color:#555555; font-size:14px;">

                                    The information in this website was created for the purposing of providing
                                    information that will help investors make informed decisions.
                                    It was not created to solicit investors to buy or sell any securities. The final
                                    decision and responsibility for investments rests solely with the user of this
                                    website and its content.
                                    Please also be aware that information on this website may be changed, modified,
                                    added or removed at any time without prior notice.
                                    Although Online Asset Company Limited has made careful efforts regarding the
                                    accuracy of the contents here, Online Asset Company limited assumes no
                                    responsibility for problems including,
                                    but not limited to, incorrect information or problems resulting from downloading of
                                    data.


                                </font> -->

                                <!------------------------------------- End Disclaimer ----------------------------------------->
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="one columns"></div>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->