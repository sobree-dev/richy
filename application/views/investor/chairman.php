<!---------------------------------- Content ---------------------------------------->
<section>

    <div class="grid-container display-main">

        <?php
        $this->db->like('groups', 'ประธานกรรมการบริษัท');
        $chairman1 = $this->db->get('chairman');
        foreach ($chairman1->result_array() as $value) {
        ?>

        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?=$value['title_'.$this->lang->lang()];?></strong>
            </h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('CorporateInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('ChairmanStatement')?></span></p>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot30">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
                    <img src="<?=base_url('uploads/chairman/'.$value['image']);?>" class="border-chairman">
                </div>
                <div class="col-lg-10 col-md-8 col-sm-12 col-xs-12 pad-text-governance">
                    <?=$value['detail_'.$this->lang->lang()];?>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php
        $this->db->like('groups', 'ประธานกรรมการบริหาร');
        $chairman2 = $this->db->get('chairman');
        foreach ($chairman2->result_array() as $value) {
        ?>

        <div class="pad-sub-detail">
            <hr>
            <br>
            <br>
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?=$value['title_'.$this->lang->lang()];?></strong>
            </h1>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
                    <img src="<?=base_url('uploads/chairman/'.$value['image']);?>" class="border-chairman">
                </div>
                <div class="col-lg-10 col-md-8 col-sm-12 col-xs-12 pad-text-governance">
                    <?=$value['detail_'.$this->lang->lang()];?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->


