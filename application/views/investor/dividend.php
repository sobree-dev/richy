<style>
    .table-data table tr td {
        padding: 20px !important;
    }
</style>
<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('DividendPolicy')?></strong>
            </h1>
            <hr>
            <p><strong><a class="a-sub-menu"
                        href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span
                    class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('ShareholderInfo')?></strong><span
                        class="font-gray-smoke" style="padding: 0 1%;">/</span><span
                        class="font-mint-green"><?php echo lang('DividendPolicy')?></span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <div>
                    <div class="row">
                        <?php
                        $dividend_info = $this->db->get('dividend_info');
                        foreach ($dividend_info->result_array() as $value) {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot30">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12"> <img
                                        src="<?=base_url('uploads/dividend_info/'.$value['image']);?>"> </div>
                                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                    <?=$value['detail_'.$this->lang->lang()];?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot30">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12">
                                    <div class="row-ir">
                                        <div class="frame-chart">
                                            <div id="highcharts-18e59108-26a7-408a-81fc-cb993af29af1"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="row-ir">
                                        <?php
                                        $this->db->limit(1);
                                        $this->db->order_by('BoD', 'DESC');
                                        $this->db->order_by('XD', 'DESC');
                                        $this->db->order_by('Dividend', 'DESC');
                                        $dividend0 = $this->db->get('dividend');
                                        foreach ($dividend0->result_array() as $value0) {
                                        ?>
                                        <div class="cover-chat">
                                            <div>
                                                <p class="text-id-year">ปี <?=date("Y",strtotime($value0['BoD']));?></p>
                                            </div>
                                            <div class="cover-text-id"> <span
                                                    class="text-id-1">จำนวนเงินปันผลต่อหุ้น</span> <span
                                                    class="text-id-2"><?=$value0['perShare'];?></span></div>
                                            <div class="cover-text-id"> <span
                                                    class="text-id-1">วันที่คณะกรรมการมีมติ</span>
                                                <span class="text-id-2"><?=$value0['BoD'];?></span> </div>
                                            <div class="cover-text-id"> <span
                                                    class="text-id-1">วันที่จ่ายเงินปันผล</span> <span
                                                    class="text-id-2"><?=$value0['Dividend'];?></span> </div>
                                            <br>
                                            <br class="hidden-md">
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div>
                                <div class="table-data">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th align="center">วันที่คณะกรรมการมีมติ</th>
                                                <th align="center">วันที่ขึ้นเครื่องหมาย</th>
                                                <th align="center">วันที่จ่ายเงินปันผล</th>
                                                <th align="center">ประเภทเงินปันผล</th>
                                                <th align="center">จำนวนเงินปันผลต่อหุ้น</th>
                                                <th align="center">วันผลประกอบการ</th>
                                            </tr>
                                            <?php
                                            $this->db->order_by('BoD', 'DESC');
                                            $this->db->order_by('XD', 'DESC');
                                            $this->db->order_by('Dividend', 'DESC');
                                            $dividend1 = $this->db->get('dividend');
                                            foreach ($dividend1->result_array() as $value1) {
                                            ?>
                                            <tr>
                                                <td align="center"><?=$value1['BoD'];?></td>
                                                <td align="center"><?=$value1['XD'];?></td>
                                                <td align="center"><?=$value1['Dividend'];?></td>
                                                <td align="center"><?=$value1['Type'];?></td>
                                                <td align="center"><?=$value1['perShare'];?></td>
                                                <td align="center"><?=$value1['Performance_'.$this->lang->lang()];?>
                                                </td>
                                            </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                    <br>
                                    <!-- <p>* Cash Compensation for fractions of: 0.07143 shares (baht per share) </p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / row -->
                </div>
                <!-- / twelve columns -->
            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->

<pre id="csv_data" style="display:none">Year,จำนวนเงินปันผลต่อหุ้น
    <?php
    $this->db->like('Type', 'Cash Dividend');
    $dividend = $this->db->get('dividend');
    foreach ($dividend->result_array() as $value) {
    ?>
        <?=date("Y",strtotime($value['BoD']));?>,<?=$value['perShare'];?>,
    <?php } ?>
</pre>

<script>
(function () {
	var files = ["https://code.highcharts.com/stock/highstock.js", "https://code.highcharts.com/highcharts-more.js", "https://code.highcharts.com/highcharts-3d.js", "https://code.highcharts.com/modules/data.js", "https://code.highcharts.com/modules/funnel.js", "https://code.highcharts.com/modules/annotations.js", "https://code.highcharts.com/modules/solid-gauge.js"],
		loaded = 0;
	if (typeof window["HighchartsEditor"] === "undefined") {
		window.HighchartsEditor = {
			ondone: [cl],
			hasWrapped: false,
			hasLoaded: false
		};
		include(files[0]);
	} else {
		if (window.HighchartsEditor.hasLoaded) {
			cl();
		} else {
			window.HighchartsEditor.ondone.push(cl);
		}
	}

	function isScriptAlreadyIncluded(src) {
		var scripts = document.getElementsByTagName("script");
		for (var i = 0; i < scripts.length; i++) {
			if (scripts[i].hasAttribute("src")) {
				if ((scripts[i].getAttribute("src") || "").indexOf(src) >= 0 || (scripts[i].getAttribute("src") === "http://code.highcharts.com/highcharts.js" && src === "https://code.highcharts.com/stock/highstock.js")) {
					return true;
				}
			}
		}
		return false;
	}

	function check() {
		if (loaded === files.length) {
			for (var i = 0; i < window.HighchartsEditor.ondone.length; i++) {
				try {
					window.HighchartsEditor.ondone[i]();
				} catch (e) {
					console.error(e);
				}
			}
			window.HighchartsEditor.hasLoaded = true;
		}
	}

	function include(script) {
		function next() {
			++loaded;
			if (loaded < files.length) {
				include(files[loaded]);
			}
			check();
		}
		if (isScriptAlreadyIncluded(script)) {
			return next();
		}
		var sc = document.createElement("script");
		sc.src = script;
		sc.type = "text/javascript";
		sc.onload = function () {
			next();
		};
		document.head.appendChild(sc);
	}

	function each(a, fn) {
		if (typeof a.forEach !== "undefined") {
			a.forEach(fn);
		} else {
			for (var i = 0; i < a.length; i++) {
				if (fn) {
					fn(a[i]);
				}
			}
		}
	}
	var inc = {},
		incl = [];
	each(document.querySelectorAll("script"), function (t) {
		inc[t.src.substr(0, t.src.indexOf("?"))] = 1;
    });
    
	function cl() {
		if (typeof window["Highcharts"] !== "undefined") {
			var options = {
				"title": {
					"text": "นโยบายจ่ายปันผล"
				},
				"subtitle": {
					"text": ""
				},
				"exporting": {},
				"series": [{
					"name": "Dividend per Share",
					"turboThreshold": 0
				}],
				"xAxis": [{
					"type": "category",
					"uniqueNames": false,
					"title": {},
					"labels": {
						"format": "{value}"
					}
				}],
				"data": {
					"csv": document.getElementById('csv_data').innerHTML,
					"googleSpreadsheetKey": false,
					"googleSpreadsheetWorksheet": false
				},
				"chart": {
					"type": "column",
					"inverted": false
				},
				"yAxis": [{
					"allowDecimals": false,
					"title": {
						"text": "Baht/Share",
						"style": "{ \"color\": \"#666666\" }"
					},
					"labels": {},
					"type": "logarithmic"
				}],
				"tooltip": {},
				"plotOptions": {
					"series": {
						"animation": false
					}
				},
				"legend": {},
				"colors": ["#1d9f68", "#00663b", "#9d7f49", "#303438", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"],
				"credits": {
					"enabled": false
				}
			};
			/*
			// Sample of extending options:
			Highcharts.merge(true, options, {
			    chart: {
			        backgroundColor: "#bada55"
			    },
			    plotOptions: {
			        series: {
			            cursor: "pointer",
			            events: {
			                click: function(event) {
			                    alert(this.name + " clicked\n" +
			                          "Alt: " + event.altKey + "\n" +
			                          "Control: " + event.ctrlKey + "\n" +
			                          "Shift: " + event.shiftKey + "\n");
			                }
			            }
			        }
			    }
			});
			*/
			new Highcharts.Chart("highcharts-18e59108-26a7-408a-81fc-cb993af29af1", options);
		}
	}
})();

</script>