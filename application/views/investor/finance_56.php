<!---------------------------------- Content ---------------------------------------->
<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Form 56-1</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Financial Info</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Form 56-1</span></p>
        </div>
        <div class="table-data">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th align="left">Company Name</td>
                        <th align="center">Year</td>
                        <th align="center">Date & Time</td>
                        <th align="center">Detail</td>
                    </tr>
                </thead>
                <tbody>

                    <tr align="left">

                        <td align="left">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</td>
                        <td align="center">
                            2018
                        </td>
                        <td align="center">20/03/2019</td>
                        <td align="center"><a href="f56_1/F1187T18.zip" target="_blank" title="Download"><img
                                    src="<?=base_url();?>images/icon_download.png" width="16px" border="0"></a></td>
                    </tr>

                    <tr align="left">

                        <td align="left">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</td>
                        <td align="center">
                            2017
                        </td>
                        <td align="center">30/03/2018</td>
                        <td align="center"><a href="f56_1/F1187E17.zip" target="_blank" title="Download"><img
                                    src="<?=base_url();?>images/icon_download.png" width="16px" border="0"></a></td>
                    </tr>

                    <tr align="left">

                        <td align="left">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</td>
                        <td align="center">
                            2016
                        </td>
                        <td align="center">31/03/2017</td>
                        <td align="center"><a href="f56_1/F1187T16.zip" target="_blank" title="Download"><img
                                    src="<?=base_url();?>images/icon_download.png" width="16px" border="0"></a></td>
                    </tr>

                    <tr align="left">

                        <td align="left">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</td>
                        <td align="center">
                            2015
                        </td>
                        <td align="center">28/03/2016</td>
                        <td align="center"><a href="f56_1/F1187T15.zip" target="_blank" title="Download"><img
                                    src="<?=base_url();?>images/icon_download.png" width="16px" border="0"></a></td>
                    </tr>

                    <tr align="left">

                        <td align="left">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</td>
                        <td align="center">
                            2014
                        </td>
                        <td align="center">31/03/2015</td>
                        <td align="center"><a href="f56_1/F1187T14.zip" target="_blank" title="Download"><img
                                    src="<?=base_url();?>images/icon_download.png" width="16px" border="0"></a></td>
                    </tr>

                </tbody>
            </table>
            <br><br>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->