<!DOCTYPE html>
<!-- saved from url=(0059)https://www.richy.co.th/th/corporate/organization_structure -->
<html class="whatinput-types-initial gr__richy_co_th whatinput-types-mouse" data-whatinput="mouse"
	data-whatintent="mouse">
<!--<![endif]-->


<!--include file = "../../check_status_of_listed.asp" -->


<!-- Mirrored from www.irplus.in.th/Listed/RICHY/home.asp by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 03 Oct 2019 09:48:42 GMT -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>นักลงทุนสัมพันธ์ Richy :: Rich in living</title>
	<meta name="keywords" content="Richy,living ">
	<meta name="description" content="Richy in living">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="https://www.richy.co.th/favicon.ico" type="image/x-icon">

	<!--ir-style-->
	<link rel="stylesheet" href="<?=base_url();?>css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url();?>css/ir-style.css">
	<link rel="stylesheet" href="<?=base_url();?>css/ir-style-old.css">
	<link rel="stylesheet" href="<?=base_url();?>css/table.css">

	<link rel="stylesheet" href="<?=base_url();?>css/style_listed.css">
	<!--Accordion-->
	<link rel="stylesheet" href="<?=base_url();?>css/smk-accordion.css">

	<!--table-slide	-->
	<link rel="stylesheet" href="<?=base_url();?>css/scroll-hint.css">

	<!-- <script src="<?=base_url();?>scripts/dividend-en.js"></script> -->

	<link rel="stylesheet" href="<?=base_url();?>css/app1.css">
	<link rel="stylesheet" href="<?=base_url();?>css/animate.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/all.css">
	<link rel="stylesheet" href="<?=base_url();?>css/lightgallery.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/lightslider.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/style.css">

	<!--loadpadge-->
	<script src="<?=base_url();?>scripts/loadpage.js"></script>

	<!--Modal-->
	<link rel="stylesheet" href="<?=base_url();?>css/modal.css">
	<script type="text/javascript" async="" src="<?=base_url();?>scripts/init.js"></script>
	<script src="<?=base_url();?>scripts/app.js"></script>


	<!--Menu-->
	<link rel="stylesheet" href="<?=base_url();?>css/component.css">
	<script type="text/javascript" src="<?=base_url();?>scripts/modernizr.custom.js"></script>

	<!--swal2-->
	<link rel="stylesheet" href="<?=base_url();?>css/swal2.css">

	<!--highchart-->

	<script src="<?=base_url();?>scripts/highcharts.js"></script>



	<!-- DateTime - Calendar -->
	<link type="text/css" rel="stylesheet" href="<?=base_url();?>css/tail.datetime-default.css" />
	<script type="text/javascript" src="<?=base_url();?>scripts/tail.datetime.js"></script>
	<script type="text/javascript" src="<?=base_url();?>scripts/tail.datetime-full.js"></script>
	<script type="text/javascript" src="<?=base_url();?>scripts/tail.datetime-all.js"></script>

	<!-- #Menu add modernizr.custom -->
	<script type="text/javascript" src="<?=base_url();?>scripts/jquery.dlmenu-en.js"></script>
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
</head>

<body>


	<?php $this->load->view('header');?>
	<?php $this->load->view('menuleft');?>
	<?php $this->load->view($content);?>
	<?php $this->load->view('footer');?>


	<script src="<?=base_url();?>scripts/lightgallery.js"></script>
	<script src="<?=base_url();?>scripts/wow.js"></script>
	<script src="<?=base_url();?>scripts/lightslider.min.js"></script>
	<script src="<?=base_url();?>scripts/sweetalert2.all.min.js"></script>

	<!--Modal-->
	<script src="<?=base_url();?>scripts/modal.js"></script>
	<div id="wh-widget-send-button" class=" wh-widget-right" style="width: 345px; height: 586px;">
		<iframe id="wh-widget-send-button-iframe"
			src="https://widget.whatshelp.io/widget/wSendButton?call=%2B6628861817&amp;facebook=327568079082&amp;line=%2F%2Fline.me%2Fti%2Fp%2F%2540gjf0556p&amp;email=contact%40rp.co.th&amp;company_logo_url=https%3A%2F%2Fwww.richy.co.th%2Fimg%2Flogo-80.jpg&amp;greeting_message=%E0%B8%AA%E0%B8%A7%E0%B8%B1%E0%B8%AA%E0%B8%94%E0%B8%B5%E0%B8%84%E0%B9%88%E0%B8%B0%20%E0%B8%AA%E0%B8%AD%E0%B8%9A%E0%B8%96%E0%B8%B2%E0%B8%A1%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%AD%E0%B8%B5%E0%B8%A2%E0%B8%94%E0%B8%AA%E0%B8%B4%E0%B8%99%E0%B8%84%E0%B9%89%E0%B8%B2%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%84%E0%B8%94%E0%B9%89%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%99%E0%B8%B5%E0%B9%88%E0%B8%84%E0%B9%88%E0%B8%B0.&amp;call_to_action=%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%AD%E0%B8%9A%E0%B8%96%E0%B8%B2%E0%B8%A1%E0%B9%80%E0%B8%9E%E0%B8%B4%E0%B9%88%E0%B8%A1%E0%B9%80%E0%B8%95%E0%B8%B4%E0%B8%A1&amp;button_color=%23129BF4&amp;position=right&amp;order=facebook%2Cline%2Ccall%2Cemail&amp;ga=false&amp;branding=true&amp;mobile=true&amp;desktop=true&amp;shift_vertical=0&amp;shift_horizontal=0&amp;domain=null&amp;key=null&amp;parentWrapperId=wh-widget-send-button&amp;clientHostname=www.richy.co.th&amp;showHelloPopup=0&amp;isMobile=0"></iframe>
	</div>

	<!-- #Menu add modernizr.custom -->
	<script type="text/javascript" src="<?=base_url();?>scripts/jquery.dlmenu.js"></script>
	<!-- <script>
		$(function () {
			$('#dl-menu').dlmenu();
		});
	</script> -->

	<script type="text/javascript">
		$(function () {
			$("#nav-sub-meeting a.here").parent().parent().show().siblings().addClass('sub-display').parent()
				.addClass('sub-active');
			$("li.sub-level-meeting div").click(function (e) {
				$("ul", $(this).toggleClass('sub-display').parent()).slideToggle();
			});
			var i = 0;
			$("li.sub-level-meeting div").each(function () {
				if (i == 0) $("ul", $(this).toggleClass('sub-display').parent()).slideToggle();
				i++;
			});
		});
	</script>
	<script>
		new WOW().init();
		$(document).ready(function () {
			$('#slidemain').lightSlider({
				//  gallery:true,
				auto: true,
				speed: 3000,
				pause: 5000,
				adaptiveHeight: true,
				item: 1,
				slideMargin: 0,
				loop: true,
				pager: false,
				responsive: [{
					breakpoint: 800,
					settings: {
						pager: true
					}
				}, ]
			});
		});
	</script>

	<!--Accordion-->
	<script type="text/javascript" src="<?=base_url();?>scripts/smk-accordion.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {

			$(".accordion_example1").smk_Accordion();

			$(".accordion_example2").smk_Accordion({
				closeAble: true, //boolean
			});

			$(".accordion_example3").smk_Accordion({
				showIcon: false, //boolean
			});

			$(".accordion_example4").smk_Accordion({
				closeAble: true, //boolean
				closeOther: false, //boolean
			});

			$(".accordion_example5").smk_Accordion({
				closeAble: true
			});

			$(".accordion_example6").smk_Accordion();

			$(".accordion_example7").smk_Accordion({
				activeIndex: 2 //second section open
			});
			$(".accordion_example8, .accordion_example9").smk_Accordion();



		});
	</script>

	<script type="text/javascript">
		try {
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost +
				"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		} catch (err) {}
	</script>
	<script type="text/javascript">
		try {
			var pageTracker = _gat._getTracker("UA-9324347-3");
			pageTracker._trackPageview();
		} catch (err) {}
	</script>
</body>


<span class="gr__tooltip">
	<span class="gr__tooltip-content"></span>
	<i class="gr__tooltip-logo"></i>
	<span class="gr__triangle"></span>
</span>


<!-- Mirrored from www.irplus.in.th/Listed/RICHY/home.asp by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 03 Oct 2019 09:49:03 GMT -->

</html>