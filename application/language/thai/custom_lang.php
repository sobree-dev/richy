<?php

$lang['home'] = "หน้าหลักนักลงทุนสัมพันธ์";
$lang['IRMenu'] = "เมนูนักลงทุนสัมพันธ์";
$lang['webcasts'] = "ข้อมูลนำเสนอแบบมัลติมีเดีย";
$lang['IRCalendar'] = "ปฏิทินกิจกรรม";
$lang['NewsActivities'] = "ข่าวสารและกิจกรรม";
$lang['Presentation'] = "ข้อมูลนำเสนอ";
$lang['Newsclipping'] = "ข่าวจากหนังสือพิมพ์";
$lang['NewsUpdate'] = "ข่าวสารล่าสุด";
$lang['SETAnnoucement'] = "ข่าวประกาศจากตลาดหลักทรัพย์";
$lang['StockInformation'] = "ข้อมูลราคาหลักทรัพย์";
$lang['StockQuote'] = "ราคาหลักทรัพย์ล่าสุด";
$lang['IRContact'] = "ติดต่อนักลงทุนสัมพันธ์";
$lang['CorporateInfo'] = "ข้อมูลบริษัท";
$lang['ChairmanStatement'] = "สารจากประธานกรรมการ";
$lang['History'] = "ประวัติความเป็นมา";
$lang['VisionMission'] = "วิสัยทัศน์และพันธกิจ";
$lang['BoardofDirector'] = "คณะกรรมการบริษัทและผู้บริหาร";
$lang['OrganizationChart'] = "โครงสร้างองค์กร";
$lang['CorporateGovernance'] = "การกำกับดูแลกิจการ";
$lang['ShareholderInfo'] = "ข้อมูลสำหรับผู้ถือหุ้น";
$lang['MeetingofShareholder'] = "การประชุมผู้ถือหุ้น";
$lang['MajorShareholder'] = "โครงสร้างผู้ถือหุ้น";
$lang['Profile'] = "ข้อมูลทั่วไปของบริษัท";
$lang['PublicRelations'] = "ข่าวประชาสัมพันธ์";
$lang['DividendPolicy'] = "นโยบายการจ่ายเงินปันผล";



